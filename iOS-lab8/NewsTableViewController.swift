//
//  NewsTableViewController.swift
//  iOS-lab8
//
//  Created by iosdev on 17.4.2020.
//  Copyright © 2020 Matias Jalava. All rights reserved.
//

import UIKit
import CoreData

class NewsTableViewController: UITableViewController, OpenDataEDCAPIDelegate {
    func newData(_ newsData: [NewsInfo]) {
        print("memes")
    }
    
    //MARK: Properties
    var news: [NSManagedObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateData(countryAbbreviation: "us", "36f6c2ec5b1d4f03b0b5845d49d0408a")
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

     //MARK: Setup of API functions
    func updateData (countryAbbreviation country: String, _ apiKey: String){
        let newsAPI = OpenDataEDCAPI()
        newsAPI.url="https://newsapi.org/v2/top-headlines?country=\(country)&apiKey=\(apiKey)"
        newsAPI.openDataEDCAPIDelegate = self
        newsAPI.getData()
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return news.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "NewsTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? NewsTableViewCell else {
            fatalError("The cell is not NewsTableViewCell")
        }

        let headLine = news[indexPath.row]
        
        // Configure the cell:
        cell.headLineLabel.text = headLine.value(forKeyPath: "headLine") as? String
        cell.sourceLabel.text = "yle"
        cell.timeLabel.text = "17:32"

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
