//
//  NewsInfo.swift
//  iOS-lab8
//
//  Created by iosdev on 19.4.2020.
//  Copyright © 2020 Matias Jalava. All rights reserved.
//

// Example of newsobject:

var newsObject: [String: Any] = [
    "source": [
         "id": "nbc-news",
         "name": "NBC News"
    ],
     "author": "Phil McCausland",
     "title": "Florida man encases arms in concrete in protest of prison conditions during pandemic - NBC News",
     "description": "A man in Florida encased his arms within barrels full of concrete outside the governor's mansion in Tallahassee on Friday in protest of the state continuing to imprison inmates amid the pandemic.",
     "url": "https://www.nbcnews.com/news/us-news/florida-man-encases-arms-concrete-protest-prison-conditions-during-pandemic-n1186886",
     "urlToImage": "https://media1.s-nbcnews.com/j/newscms/2020_16/3313851/200418-jordan-mazurek-al-1029_1a909af282d289e09e1a0710c7ecd4c4.nbcnews-fp-1200-630.jpg",
     "publishedAt": "2020-04-18T15:20:52Z",
     "content": "A man in Florida encased his arms in barrels full of concrete outside the governor's mansion in Tallahassee on Friday in protest of the state's continuing to hold prisoners amid the coronavirus pandemic.\r\nTallahassee police arrested Jordan Mazurek, 28, around… [+2056 chars]"
 ]

import Foundation

struct NewsInfo:Codable{
    var source: [String]?
    var author: String?
    var title: String?
    var description: String?
    var url: String?
    var imageUrl: String?
    var publishedAt: String?
    var content: String?
    
    private enum CodingKeys:String, CodingKey{
        case source
        case author
        case title
        case description
        case url
        case imageUrl
        case publishedAt
        case content
    }
};

/*
struct source:Codable{
    var id: String?
    var name: String?
}
 */
