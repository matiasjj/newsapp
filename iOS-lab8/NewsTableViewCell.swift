//
//  News.swift
//  iOS-lab8
//
//  Created by iosdev on 17.4.2020.
//  Copyright © 2020 Matias Jalava. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {
    
    //MARK: Initialazion
    
    @IBOutlet weak var headLineLabel: UILabel!
    @IBOutlet weak var sourceLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
