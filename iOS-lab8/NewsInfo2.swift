//
//  NewsInfo2.swift
//  iOS-lab8
//
//  Created by iosdev on 19.4.2020.
//  Copyright © 2020 Matias Jalava. All rights reserved.
//

import Foundation

struct NewsInfo2:Codable{
    var source: source
    var author: String?
    var title: String?
    var description: String?
    var url: String?
    var imageUrl: String?
    var publishedAt: String?
    var content: String?
    
    private enum CodingKeys:String, CodingKey{
        case source
        case author
        case title
        case description
        case url
        case imageUrl
        case publishedAt
        case content
    }
};

struct source:Codable{
    var id: String?
    var name: String
}
