//
//  OpenDataEDCApi.swift
//  iOS-lab8
//
//  Created by iosdev on 19.4.2020.
//  Copyright © 2020 Matias Jalava. All rights reserved.
//

import Foundation

class OpenDataEDCAPI{
    
    private var _url: URL?
    var openDataEDCAPIDelegate: OpenDataEDCAPIDelegate?

    
    // Example of newsobject:
    
    var newsDictionary: [String: Any] = [
        "source": [
            "id": "nbc-news",
            "name": "NBC News"
        ],
        "author": "Phil McCausland",
        "title": "Florida man encases arms in concrete in protest of prison conditions during pandemic - NBC News",
        "description": "A man in Florida encased his arms within barrels full of concrete outside the governor's mansion in Tallahassee on Friday in protest of the state continuing to imprison inmates amid the pandemic.",
        "url": "https://www.nbcnews.com/news/us-news/florida-man-encases-arms-concrete-protest-prison-conditions-during-pandemic-n1186886",
        "urlToImage": "https://media1.s-nbcnews.com/j/newscms/2020_16/3313851/200418-jordan-mazurek-al-1029_1a909af282d289e09e1a0710c7ecd4c4.nbcnews-fp-1200-630.jpg",
        "publishedAt": "2020-04-18T15:20:52Z",
        "content": "A man in Florida encased his arms in barrels full of concrete outside the governor's mansion in Tallahassee on Friday in protest of the state's continuing to hold prisoners amid the coronavirus pandemic.\r\nTallahassee police arrested Jordan Mazurek, 28, around… [+2056 chars]"
    ]
    
    var url: String {
        
        // .addingPercentEncoding makes sure the setter doesn't crash the application. Before that it surely did when url contained special letters
        set{
            print("url going to be: \(newValue)")
            if let encoded = newValue.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                let enc = URL(string: encoded){
                _url = enc
            }
        }
        get{
            return _url?.absoluteString ?? "no value"
        }
    }
    
    func getData () {
        let dataTask = URLSession.shared.dataTask(with: _url!) { data, response, error in
            if let error = error{
                print("error: ", error)
            }
            
            guard let httpResponse = response as? HTTPURLResponse,
                (200...299).contains(httpResponse.statusCode) else {
                    print("douldn't connect")
                    return
            }
            
            guard let data = data else {
                print("couldn't get data")
                return
            }
            
            //print("data: ", data)
            
            guard let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else {
                print("Couldn't get data")
                return
            }
            print(json)
            
            // This isn't working atm
            let decoder = JSONDecoder()
            guard let info: [NewsInfo] = try? decoder.decode([NewsInfo].self, from: data) else {
                print("Couldn't decode data, ", data)
                return
            }
            print("main info: \(info)")
            
            self.openDataEDCAPIDelegate?.newData(info)
            
            //self.openDataEDCAPIDelegate?.newData(json)
        }
        dataTask.resume()
    }
}

protocol OpenDataEDCAPIDelegate {
    func newData(_ newsData: [NewsInfo])
}
